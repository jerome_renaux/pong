/**
 * Created by Jerome on 14-08-16.
 */
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);

app.use('/assets',express.static(__dirname + '/assets'));
app.use('/js',express.static(__dirname + '/js'));
app.use('/css',express.static(__dirname + '/css'));
//app.use('/fonts',express.static(__dirname + '/fonts'));

app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html');
});

server.listen(process.env.PORT || 8081,function(){
    console.log('Listening on '+server.address().port);
});

var pool = [];
var players = {};
var currentRoomID = 0;
var bonusTimers = {};

var Player = function(id,oid,room){
    this.id = id;
    this.opponent = oid;
    this.room = room;
};


io.on('connection',function(socket) {
    socket.on('ready',function(){
        console.log(socket.id+' has joined');
        while(includes(pool,socket.id)){
            removeValue(pool,socket.id);
        }
        pool.push(socket.id);
        socket.join(currentRoomID);

        console.log(pool);
        if(pool.length >= 2){
            console.log('Starting match between '+pool[0]+' and '+pool[1]);
            makePlayer(pool[0],pool[1],currentRoomID);
            makePlayer(pool[1],pool[0],currentRoomID);
            io.in(pool[0]).emit('start',{haveBall:true});
            io.in(pool[1]).emit('start',{haveBall:false});
            removeValue(pool,pool[0]);
            removeValue(pool,pool[1]);
            bonusTimers[currentRoomID] = setInterval(decideBonus, 5000,currentRoomID);
            currentRoomID++;
        }
    });

    socket.on('keypress',function(data){
        io.in(players[socket.id].opponent).emit('keypress',data);
    });

    socket.on('fire',function(){
        io.in(players[socket.id].opponent).emit('fire');
    });

    socket.on('disconnect',function(){
        console.log(socket.id+' has left');
        removeValue(pool,socket.id);
        if(players[socket.id]) {
            removeValue(pool, players[socket.id].opponent);
            console.log('Clearing '+players[socket.id].room);
            clearInterval(bonusTimers[players[socket.id].room]);
            io.in(players[socket.id].opponent).emit('wait');
        }
    })
});

function makePlayer(p1,p2,room){
    players[p1] = new Player(p1,p2,room);
}

function decideBonus(room){
    var b = Math.floor(Math.random()*5);
    //console.log('Emiting bonus in room '+room);
    io.in(room).emit('bonus',{type:b});
}

function removeValue(array,val){
    var idx = array.indexOf(val);
    if(idx > -1) {
        array.splice(idx,1);
    }
}

function includes(array,val) {
    return (array.indexOf(val) !== -1);
}