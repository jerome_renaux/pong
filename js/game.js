/**
 * Created by Jerome on 03-03-16.
 */
/*
 * Author: Jerome Renaux
 * E-mail: jerome.renaux@gmail.com
 */

function Pad(side) {
    this.sprite = null;
    this.speed = 400;
    this.padSize = 1; // 0 = small, 1 = normal, 2 = big
    this.keys = {}; // keyboard keys used to move the pad
    this.bonuses = []; // active bonuses on the pad, useful to keep track of them
    this.side = side;
}

function Bonus(_id,_tween,_type){
    this.id = _id;
    this.tween = _tween;
    this.bonus = _type;
}

var Game = {
    rightPad : new Pad('right'),
    leftPad : new Pad('left'),
    trackPad : null, // which pad holds the ball
    trackedObject : null, // what the left pad will track in 1 player mode
    ball: {
        sprite: null
    },
    bonuses : {
        sprites: []
    },
    cursors : {},
    bonusDuration : 10000, // ms
    lastBonusID : 0,
    nbPlayers : 0,
    online: false
};

Game.preload = function() {
    game.load.spritesheet('voidpx','assets/void.png',1,1);
    game.load.spritesheet('bonuses','assets/bonuses.png',40,40);
    // Pack using https://www.leshylabs.com/apps/sstool/, JSON-TP-HASH mode
    game.load.atlasJSONHash('pads', 'assets/pads.png', 'assets/pads.json');
};

Game.create = function(){
    game.physics.startSystem(Phaser.Physics.ARCADE);

    var rightPadX = game.world.width-30;
    var leftPadX = 5;
    Game.pads = game.add.group();
    Game.pads.enableBody = true;
    Game.rightPad.sprite = Game.pads.create(rightPadX, game.world.height/2, 'pads','padright');
    Game.leftPad.sprite = Game.pads.create(leftPadX, game.world.height/2, 'pads','padleft');
    // Some callbacks only receive the sprite object ; it is sometimes useful to be able to access the parent pad object from it
    Game.rightPad.sprite.parentObj = Game.rightPad;
    Game.leftPad.sprite.parentObj = Game.leftPad;
    Game.pads.setAll('anchor.y',0.5);
    Game.pads.setAll('body.collideWorldBounds',true);
    Game.pads.setAll('body.bounce',0); // So the pad doesn't bounce off the ball
    //Game.pads.setAll('body.immovable',true); // no good collision handling between groups of immovable sprites

    // Add some invisible borders to constrain the pads movements. The middle one delimits the two terrains.
    // The left and right ones are needed to prevent the pads from leaving the scene
    // (Needed because bellow, world bounds collisions for left and right are disabled to allow the ball to disappear
    Game.borders = game.add.group();
    Game.borders.enableBody = true;
    Game.borders.add(game.add.tileSprite(game.world.width/2,0,1,game.world.height,'voidpx')); // in preload, specify width and height of sprite if you wish to use it for a tilesprite
    Game.borders.add(game.add.tileSprite(0,0,5,game.world.height,'voidpx'));
    Game.borders.add(game.add.tileSprite(game.world.width-5,0,5,game.world.height,'voidpx'));
    Game.borders.setAll('body.immovable',true);

    Game.bonuses.sprites = game.add.group();

    Game.balls = game.add.group(); // Plural because a bonus can increase the number of balls in game
    if(!Game.online) {
        Game.trackPad = (Math.random() > 0.5 ? Game.leftPad : Game.rightPad);
    }
    Game.addBall(true);  // true = attach the ball to the trackPad

    Game.rightPad.keys = game.input.keyboard.createCursorKeys();
    if(Game.nbPlayers == 2) {
        Game.leftPad.keys.up = game.input.keyboard.addKey(Phaser.Keyboard.S);
        Game.leftPad.keys.right = game.input.keyboard.addKey(Phaser.Keyboard.C);
        Game.leftPad.keys.down = game.input.keyboard.addKey(Phaser.Keyboard.X);
        Game.leftPad.keys.left = game.input.keyboard.addKey(Phaser.Keyboard.W);
    }
    Game.cursors.fire = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    // To allow the ball to leave the scene on the left and right sides
    game.physics.arcade.checkCollision.left = false;
    game.physics.arcade.checkCollision.right = false;

    Game.scoreLeftTxt = game.add.bitmapText(0, 0, 'videogame', '0',64);
    game.add.bitmapText((game.world.width/2)-32, 0, 'videogame', '-',64);
    Game.scoreRightTxt = game.add.bitmapText(0, 0, 'videogame', '0',64);
    Game.score.left = 0;
    Game.score.right = 0;
    Game.updateScores();

    var middleSeparator = game.add.graphics(game.world.width/2, 0);
    middleSeparator.lineStyle(3, 0xffffff, 1);
    middleSeparator.lineTo(0,game.world.height);
    /*var line = new Phaser.Line(game.world.width/2, 0, game.world.width/2, game.world.height);
    game.debug.geom(line);*/

    // Loop to spawn bonuses
    if(!Game.online) {
        Game.timer = game.time.events;
        Game.bonusLoop = Game.timer.loop(Phaser.Timer.SECOND * 5, Game.decideBonus, this);
        Game.timer.start();
    }
};

Game.decideBonus = function(){ // Make a new bonus appear at the top of the screen
    if(!Game.trackPad){
        var b = Math.floor(Math.random()*5);
        Game.spawnBonus(b);
    }
};

Game.spawnBonus = function(type){
    if(Game.trackPad){return;}
    var bonusesMap = {
        0 : 'speedup',
        1 : 'enlarge',
        2 : 'triball',
        3 : 'speedown',
        4 : 'small'
    };
    var bonus = game.add.sprite(Math.random()*(game.world.width-40),0,'bonuses',type);
    game.physics.arcade.enable(bonus);
    bonus.body.velocity.y = 50;
    bonus.checkWorldBounds = true;
    bonus.events.onOutOfBounds.add(function(_bonus){
        _bonus.destroy();
    }, this);
    bonus.type = bonusesMap[type];
    bonus.bonusClass = (type < 3 ? 'bonus' : 'malus');
    Game.bonuses.sprites.add(bonus);
};

Game.catchBonus = function(_pad,_bonus){
    if(Game.trackPad){return true;} // A pad cannot get a bonus while holding the ball
    _bonus.destroy();
    // If the pad is at max/min size and catches a enlarge/small bonus, renew the ongoing enlarge/small bonus instead of applying a new one
    if((_pad.parentObj.padSize == 2 && _bonus.type == 'enlarge') || (_pad.parentObj.padSize == 0 && _bonus.type == 'small')) {
        if(_bonus.type == 'enlarge'){
            Game.renewBonus(_pad.parentObj,_bonus.type);
        }
        return;
        // If the pad is at max/min size and catches a small/enlarge bonus, they cancel each other instead of applying a new one
    }else if((_pad.parentObj.padSize == 2 && _bonus.type == 'small') || (_pad.parentObj.padSize == 0 && _bonus.type == 'enlarge')) {
        Game.endBonus(_pad.parentObj,(_bonus.type == 'small' ? 'enlarge' : 'small'));
        return;
    }
    // speedup and speedown bonuses cancel each other
    if(_bonus.type == 'speedup' || _bonus.type == 'speedown') {
        if(Game.endBonus(_pad.parentObj,(_bonus.type == 'speedup' ? 'speedown' : 'speedup'))){
            return;
        }
    }
    var makeBar = Game.applyBonus(_bonus.type,_pad.parentObj);
    if(makeBar) {
        var bar = Game.addProgressBar(_pad,_bonus.bonusClass);
        var tween = Game.makeTween(bar,_pad.parentObj,_bonus.type);
        _pad.parentObj.bonuses.push(new Bonus(bar.id,tween,_bonus.type));
    }
};

Game.makeTween = function(bar,pad,bonus){ // Create the tween on the progress bar of the bonus
    var tween = game.add.tween(bar);
    tween.to({value: 0}, Game.bonusDuration, null, false, 0);
    tween.onComplete.add(function () {
        document.getElementById('game').removeChild(bar);
        Game.revertBonus(bonus,pad);
        Game.removeBonus(pad,bar.id);
    }, this);
    tween.start();
    return tween;
};

Game.renewBonus = function(pad,bonus){ // Fill the progress bar of an ongoing bonus
    for(var i = 0; i < pad.bonuses.length; i++){
        if(pad.bonuses[i].bonus == bonus){
            var bar = pad.bonuses[i].tween.target;
            pad.bonuses[i].tween.stop(false); // false means do not fire onComplete (that would kill the bar)
            bar.value = 100;
            pad.bonuses[i].tween = Game.makeTween(bar,pad,bonus);
            break;
        }
    }
};

Game.endBonus = function(pad,bonus){ // Prematurely ends a bonus and fire the onComplete method to kill the bar and disable the effect
    for(var i = 0; i < pad.bonuses.length; i++){
        if(pad.bonuses[i].bonus == bonus){
            break;
        }
    }
    if(i == pad.bonuses.length){return false;}
    pad.bonuses[i].tween.stop(true); // true = fire onComplete
    return true;
};

Game.removeBonus = function(pad,id){ // Remove a bonus from the list of ongoing bonuses in the Pad object
   for(var i = 0; i < pad.bonuses.length; i++){
       if(pad.bonuses[i].id == id){
            break;
       }
   }
    if(i == pad.bonuses.length){return;}
    pad.bonuses.splice(i);
};

Game.applyBonus = function(bonus,pad){ // Applies a bonus effect ; returns whether or not a progress bar should be created
    if(bonus == 'speedup'){
        pad.speed *= 1.5;
        if(pad.speed > 1000){pad.speed = 1000;}
        return true;
    }else if(bonus == 'speedown'){
        pad.speed /= 1.5;
        return true;
    }else if(bonus == 'triball') {
        var tmpGroup = game.add.group();
        Game.balls.moveAll(tmpGroup);
        tmpGroup.forEach(Game.multiplyBall, this, true);
        tmpGroup.removeChildren();
        return false;
    }else if(bonus == 'enlarge' || bonus == 'small'){
        if((pad.padSize < 2 && bonus == 'enlarge') || (pad.padSize > 0 && bonus == 'small')) {
            var newPadSize = (bonus == 'enlarge' ? pad.padSize+1 : pad.padSize-1);
            Game.updatePadSize(pad,newPadSize);
            return true;
        }
    }
};

Game.revertBonus = function(bonus,pad) { // Cancels the effect of a bonus when it runs out of time
    if(bonus == 'speedup'){
        pad.speed /= 1.5;
    }else if(bonus == 'speedown'){
        pad.speed *= 1.5;
    }else if(bonus == 'enlarge' || bonus == 'small'){
        if((pad.padSize > 0 && bonus == 'enlarge') || (pad.padSize < 2 && bonus == 'small')) {
            var newPadSize = (bonus == 'enlarge' ? pad.padSize-1 : pad.padSize+1);
            Game.updatePadSize(pad,newPadSize);
            return true;
        }
    }
};

Game.updatePadSize = function(pad,newPadSize){ // Make pad grow or shrink
    var frames = {
      0: 'smallpad',
        1: 'pad',
        2: 'bigpad'
    };
    var sizes = {
        0 : [24,84],
        1 : [24,96],
        2 : [24,192]
    };
    pad.sprite.frameName = frames[newPadSize]+(pad.side);
    pad.sprite.body.setSize(sizes[newPadSize][0], sizes[newPadSize][1]);
    pad.padSize = newPadSize;
};

Game.addProgressBar = function(pad,bonusClass){
    var width = 50;
    var height = 10;
    var bar = document.createElement('progress');
    bar.style.position = 'absolute';
    bar.style.width = width+'px';
    bar.style.height = height+'px';
    if(pad.parentObj.side == 'left') {
        bar.style.left = '10px';
    }else {
        bar.style.left = (game.world.width - 10 - width) + 'px';
        bar.style.transform = 'scaleX(-1)';
    }
    bar.className = bonusClass;
    bar.id = Game.lastBonusID++;
    bar.style.top = (game.world.height + (-10 - height)*(pad.parentObj.bonuses.length+1))+'px';
    bar.max = 100;
    bar.value = 100;
    document.getElementById('game').appendChild(bar);
    return bar;
};

Game.updateScores = function(){
    Game.scoreLeftTxt.text = Game.score.left;
    Game.scoreRightTxt.text = Game.score.right;
    Game.scoreLeftTxt.x = (game.world.width/2)-36-Game.scoreLeftTxt.textWidth;
    Game.scoreRightTxt.x = (game.world.width/2)+50;
};

Game.multiplyBall = function(ball) {
    for (var i = 0; i < 2; i++) {
        var newball = game.add.sprite(ball.x, ball.y, 'pads','ball');
        newball.anchor.y = 0.5;
        game.physics.arcade.enable(newball);
        newball.body.velocity.setTo(ball.body.velocity.x, Math.random() * 400);
        Game.setBallProperties(newball);
        Game.balls.add(newball);
    }
    Game.balls.add(ball);
};

Game.setBallProperties = function(ball){
    ball.body.bounce.setTo(1, 1);
    ball.checkWorldBounds = true;
    ball.body.collideWorldBounds = true;
    ball.events.onOutOfBounds.add(Game.score, this);
};

Game.addBall = function(newBall) {
    var right_offset = -10;
    var left_offset = 20;
    var x = (newBall ? (Game.trackPad == Game.leftPad ? left_offset : right_offset) : (Game.trackPad == Game.leftPad ? Game.leftPad.sprite.x + left_offset : Game.rightPad.sprite.x + right_offset) );
    var y = (newBall ? 0 : Game.trackPad.sprite.y );
    var ball = game.add.sprite(x, y, 'pads','ball');
    ball.anchor.y = 0.5;
    if(newBall){ // Anchor ball to trackPad
        Game.trackPad.sprite.addChild(ball);
        Game.trackedObject = Game.trackPad;
    }else{ // Free ball
        var speed = 400;
        var velocity = (Game.trackPad == Game.leftPad ? speed : -speed);
        game.physics.arcade.enable(ball);
        ball.body.velocity.x = velocity;
        Game.setBallProperties(ball);
        Game.balls.add(ball);
        Game.trackPad = null;
        Game.trackedObject = ball;
    }
};

Game.score = function(ball){
    var x = ball.x;
    if(x < 0){
        Game.score.right++;
    }else if(ball.x > game.world.width){
        Game.score.left++;
    }
    Game.updateScores();
    if(Game.trackedObject == ball){
        Game.trackedObject = Game.balls.getRandom();
    }
    ball.destroy();
    if(Game.balls.length == 0) { // No more balls in game
        if(x < 0){
            Game.trackPad = Game.rightPad;
            Game.removeBars(Game.leftPad);
        }else if(ball.x > game.world.width){
            Game.trackPad = Game.leftPad;
            Game.removeBars(Game.rightPad);
        }
        Game.addBall(true);
        Game.pauseTweens(Game.leftPad);
        Game.pauseTweens(Game.rightPad);
    }
};

Game.pauseTweens = function(pad){
    for(var i = 0; i < pad.bonuses.length; i++){
        pad.bonuses[i].tween.pause();
    }
};

Game.resumeTweens = function(pad){
    for(var i = 0; i < pad.bonuses.length; i++){
        pad.bonuses[i].tween.resume();
    }
};

Game.removeBars = function(pad){
    for(var i = 0; i < pad.bonuses.length; i++){
        pad.bonuses[i].tween.stop(true); // true means fire onComplete
    }
    pad.bonuses=[];
};

Game.keyFlags = {
    vertical:0,
    horizontal:0
};

Game.emitKeyPresses = function(axis,value){
    if(Game.online) {
        if (Game.keyFlags[axis] != value) {
            Game.keyFlags[axis] = value;
            //console.log('Setting ' + key + ' to ' + value);
            socket.emit('keypress',{axis:axis,value:value});
        }
    }
};

Game.movePad = function(pad){
    if (pad.keys.up.isDown){
        pad.sprite.body.velocity.y = -pad.speed;
        Game.emitKeyPresses('y',-1);
    }else if (pad.keys.down.isDown){
        pad.sprite.body.velocity.y = pad.speed;
        Game.emitKeyPresses('y',1);
    }else{
        pad.sprite.body.velocity.y = 0;
        Game.emitKeyPresses('y',0);
    }
    if (pad.keys.left.isDown){
        pad.sprite.body.velocity.x = -pad.speed;
        Game.emitKeyPresses('x',-1);
    }else if (pad.keys.right.isDown){
        pad.sprite.body.velocity.x = pad.speed;
        Game.emitKeyPresses('x',1);
    }else{
        pad.sprite.body.velocity.x = 0;
        Game.emitKeyPresses('x',0);
    }
};

Game.secondPlayerPad = function(axis,value){
    var v = (axis == 'x' ? -value : value);
    Game.leftPad.sprite.body.velocity[axis] = v*Game.leftPad.speed;
};


Game.computerPad = function(){ // Manages the movements of the computer-controlled pad in 1 player mode
    // Track the ball or enemy pad
    var target_y = (Game.trackedObject == Game.rightPad ? Game.trackedObject.sprite.y : Game.trackedObject.y);
    Game.setVelocity('y',target_y);
    // Track a bonus, if any
    var bonusFound = false;
    Game.bonuses.sprites.iterate('bonusClass','bonus',0,function(child){
        var target_x = child.x;
        if(target_x < game.world.width/2) {
            bonusFound = true;
            Game.setVelocity('x',target_x);
        }
    });
    // If no bonus found, go back to the back
    if(!bonusFound){
        var target_x = 10;
        Game.setVelocity('x',target_x);
    }
};


Game.setVelocity = function(coord,targetCoord){ // Set the velocity of the computer-controlled pad based on some target position
    var delta = targetCoord - Game.leftPad.sprite[coord];
    if(Math.abs(delta) < 20){ // To prevent jitter
        Game.leftPad.sprite.body.velocity[coord] = 0;
    }else {
        var dir = Math.sign(delta);
        Game.leftPad.sprite.body.velocity[coord] = dir * Game.leftPad.speed;
    }
};

Game.ballPadCollision = function(ball, pad) {
    var coef = 500;
    var overlapCoef = 8;
    var angle = Math.abs(game.physics.arcade.angleBetween(ball, pad));
    var x_dir = Math.cos(angle)*-1;
    var y_dir = Math.sin(angle);
    // Pushes the ball away a bit to prevent it to be dragged by a the pad if it moves right or left
    ball.x = ball.x+(x_dir*overlapCoef);
    ball.y = ball.y+(y_dir*overlapCoef);
    ball.body.velocity.setTo(x_dir*coef,y_dir*coef);
    ball.body.velocity.setTo(x_dir*coef,y_dir*coef);
};

Game.fire = function(emit){ // Shoot the ball from the pad that holds it
    Game.trackPad.sprite.removeChildren();
    Game.addBall(false);
    Game.resumeTweens(Game.leftPad);
    Game.resumeTweens(Game.rightPad);
    if(Game.online && emit){socket.emit('fire');}
};

Game.update = function(){
    game.physics.arcade.collide(Game.balls, Game.pads,Game.ballPadCollision);
    game.physics.arcade.collide(Game.borders, Game.pads);
    game.physics.arcade.overlap(Game.pads, Game.bonuses.sprites,Game.catchBonus); // overlap because we don't want the bonuses to bounce when they are not absorbed
    Game.movePad(Game.rightPad);
    if(Game.nbPlayers == 2) {
        if(Game.online){

        }else{
            Game.movePad(Game.leftPad);
        }
    }else{
        if(Game.trackPad == Game.leftPad) {
            Game.fire(false);
        }else{
            Game.computerPad();
        }
    }
    if(Game.cursors.fire.isDown && Game.trackPad && (Game.nbPlayers == 2 || Game.trackPad == Game.rightPad)){
        Game.fire(true); // true = emit fire signal to sever
    }
};