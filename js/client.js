/**
 * Created by Jerome on 16-08-16.
 */
var socket = io.connect();

// Change game state once two players are available
socket.on('start',function(data){
    console.log('Starting game');
    game.state.start('Game');
    if(data.haveBall){
        Game.trackPad = Game.rightPad;
    }else{
        Game.trackPad = Game.leftPad;
    }
});

socket.on('wait',function(){
    game.state.start('Wait');
});

socket.on('keypress',function(data){
    Game.secondPlayerPad(data.axis,data.value);
});

socket.on('fire',function(data){
    Game.fire(false); // false = don't emit fire signal to server
});

socket.on('bonus',function(data){
    Game.spawnBonus(data.type);
});