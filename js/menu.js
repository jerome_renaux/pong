/*
 * Author: Jerome Renaux
 * E-mail: jerome.renaux@gmail.com
 */

var Menu = {};

Menu.preload = function(){
    game.stage.disableVisibilityChange = true; // Stay alive evn if window loses focus
    game.load.bitmapFont('videogame', 'assets/fonts/carrier_command.png', 'assets/fonts/carrier_command.xml'); // converted from ttf using http://kvazars.com/littera/
    game.load.spritesheet('buttons','assets/buttons.png',100,100);

};

Menu.create = function(){
    var welcome = game.add.bitmapText(0, 100, 'videogame', 'WELCOME',64);
    welcome.x = (game.world.width/2)-(welcome.textWidth/2);
    var onePlayer = game.add.button(game.world.width/4, 300, 'buttons', function(){
        startGame(1);
    }, this);
    var twoPlayer = game.add.button((2*(game.world.width/4)), 300, 'buttons', function(){
        startGame(2,false);
    }, this,1,1,1,1);
    var online = game.add.button((3*(game.world.width/4)), 300, 'buttons', function(){
        startGame(2,true);
    }, this,2,2,2,2);
    onePlayer.anchor.setTo(0.5);
    twoPlayer.anchor.setTo(0.5);
    online.anchor.setTo(0.5);
    game.add.bitmapText(100, 400, 'videogame', 'Left keys : W, S, X, C',18);
    game.add.bitmapText(100, 450, 'videogame', 'Right keys : Left, Up, Right, Down',18);
    game.add.bitmapText(100, 500, 'videogame', 'Shoot ball : spacebar',18);
};

function startGame(nbplayers,online){
    Game.nbPlayers = nbplayers;
    Game.online = online;
    if(online){
        game.state.start('Wait');
    }else {
        game.state.start('Game');
    }
}