/**
 * Created by Jerome on 03-03-16.
 */

// TODO: "p2p" mode (bonuses), authoritative server mode (ball assignment, physics, score, bonuses), establish connection only in PvP, test 4 players at once


//noinspection JSCheckFunctionSignatures,JSCheckFunctionSignatures,JSCheckFunctionSignatures
var game = new Phaser.Game(900, 600, Phaser.AUTO, document.getElementById('game'));

game.state.add('Menu',Menu);
game.state.add('Wait',Wait);
game.state.add('Game',Game);

game.state.start('Menu');