/**
 * Created by Jerome on 15-08-16.
 */

var Wait = {};

Wait.create = function(){
    game.add.text(270, 220, 'Waiting for a second player', { font: "30px Arial", fill: "#19de65"});
    socket.emit('ready');
};
